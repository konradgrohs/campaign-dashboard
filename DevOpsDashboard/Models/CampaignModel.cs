﻿using AlertSense.Aspen.Common.Models;
using DevOpsDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DevOpsDashboard.Models
{
    public class CampaignModel
    {
        public int ID { get; set; }
        public List<CampaignReportResponse> ReportResponses { get; set; }
        public List<CampaignDetail> CampDetailList { get; set; }
        public IEnumerable<ErrorModel> Errors { get; set; }
    }
}