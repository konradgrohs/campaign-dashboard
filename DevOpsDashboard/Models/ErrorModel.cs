﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DevOpsDashboard.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public string ErrorCode { get; set; }

    }
}
