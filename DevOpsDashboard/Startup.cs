﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DevOpsDashboard.Startup))]
namespace DevOpsDashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
