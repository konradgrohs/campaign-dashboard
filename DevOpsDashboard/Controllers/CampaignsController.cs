﻿using AlertSense.Aspen.Client;
using AlertSense.Aspen.Client.Configuration;
using AlertSense.Aspen.Common.Models;
using AlertSense.Aspen.Common.Entities;
using DevOpsDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Net;
namespace DevOpsDashboard.Controllers
{
    public class CampaignsController : Controller
    {
        
        AspenClient client = new AspenClient();
        // GET: /Campaigns/
        public ActionResult Index(int? page, int? pageSize, string orderBy, string filterBy)
        {
            //Set default behavior
            orderBy = "CreatedOn";
            var pageNumber = page ?? 1; 
            var sizeOfPage = pageSize ?? 15; // Has to be between 10 and 100!
            CampaignDetailList campaignLstRequest;

            #region Filtering and Ordering campaigns
            if (!String.IsNullOrEmpty(orderBy) && !String.IsNullOrEmpty(filterBy))
            {
                campaignLstRequest = new CampaignDetailList
                {
                    PageNumber = pageNumber,
                    PageSize = sizeOfPage,
                    OrderBy = orderBy,
                    FilterBy = filterBy


                };
            }
            else if (!String.IsNullOrEmpty(orderBy))
            {
                campaignLstRequest = new CampaignDetailList
                {
                    PageNumber = pageNumber,
                    PageSize = sizeOfPage, //PageSize = sizeOfPage-1,
                    OrderBy = orderBy,
                    OrderDescending = true

                };
            }
            else if (!String.IsNullOrEmpty(filterBy))
            {
                campaignLstRequest = new CampaignDetailList
                {
                    PageNumber = pageNumber,
                    PageSize = sizeOfPage,
                    FilterBy = filterBy
                };
            }
            else
            {
                campaignLstRequest = new CampaignDetailList
                {
                    PageNumber = pageNumber,
                    PageSize = sizeOfPage //PageSize = sizeOfPage-1
                };
            }
            #endregion
            //TODO: FIX = Assume 35 total campaigns with page size 15..
            //              -First page index range 0-15   (16 campaigns)
            //              -Second page index range 15-30  (16 campaigns)
            //              -Third page index range 30-36 (6 campaigns)
            var CampaignDetailListResponse = client.Get(campaignLstRequest);

            #region Error Handling for DetailListRequest
            if (CampaignDetailListResponse.ResponseStatus != null)
            {
                //Error occured
                return View(
                    new CampaignModel 
                    {
                        Errors = CampaignDetailListResponse.ResponseStatus.Errors.Select(e => new ErrorModel {ErrorCode = e.ErrorCode, Message = e.Message })
                    });
            }
            #endregion

            //Campaign Paging Setup
            var remainder = CampaignDetailListResponse.TotalCount % (sizeOfPage);    //var remainder = CampaignDetailListResponse.TotalCount % (sizeOfPage-1);             
            var NumPages = CampaignDetailListResponse.TotalCount / (sizeOfPage);    //var NumPages = CampaignDetailListResponse.TotalCount / (sizeOfPage);

            if (remainder != 0) NumPages++;
            ViewBag.NumPages = NumPages;
            ViewBag.CurrentPage = pageNumber;
            ViewBag.PageSize = sizeOfPage;

            #region Populate a list of responses
            Guid campaignId;
            CampaignReportRequest request;
            var guidIDs = new List<string>();
            var reportResponses = new List<CampaignReportResponse>();

            guidIDs.AddRange(CampaignDetailListResponse.Campaigns.Select(c => c.Id.ToString()));
            
            for (int i = 0; i < guidIDs.Count(); i++)
            {
                campaignId = Guid.Parse(guidIDs.ElementAt(i));
                request = new CampaignReportRequest { CampaignId = campaignId };
                var campListResponse = client.Get(request);
                #region Error Handling for Campaign List Request
                if (campListResponse.ResponseStatus != null)
                {

                    return View(
                        new CampaignModel
                        {
                            Errors = campListResponse.ResponseStatus.Errors.Select(e => new ErrorModel { ErrorCode = e.ErrorCode, Message = e.Message })
                        });
                }
                #endregion
                reportResponses.Add(client.Get(request));
            }

            #endregion

            return View(new CampaignModel { ReportResponses = reportResponses, CampDetailList = CampaignDetailListResponse.Campaigns.ToList() });
        }

        public ActionResult Details(string campID, int? id, int? callPage, int? textPage, int? pageSize)
        {
            // Get all details from the current campaign
            Guid campaignId = Guid.Parse(campID);
            var request = new CampaignReportRequest { CampaignId = campaignId };
            var campaignreportresponse = client.Get(request);
            if (campaignreportresponse == null)
            {   // Error occurred while retrieving response
                return HttpNotFound();
            }

         // Below we get the setup for paging functionality for calls and texts

            // Process all calls associated with the campaign
            var CallsLst = new List<CallRecord>();
            var CallsQry = from c in campaignreportresponse.Calls
                           orderby c.CreatedOn
                           select c;
            CallsLst.AddRange(CallsQry);

            // Process all texts associated with the campaign
            var TextsLst = new List<TextRecord>();
            var TextsQry = from t in campaignreportresponse.Texts
                           orderby t.CreatedOn
                           select t;
            TextsLst.AddRange(TextsQry);

            #region Paging for calls and texts
            var callPageNumber = callPage ?? 1;
            var textPageNumber = textPage ?? 1;
            ViewBag.textPageNumber = textPageNumber;
            ViewBag.callPageNumber = callPageNumber;
            var sizeOfPage = pageSize ?? 25;

            var onePageOfCalls = CallsLst.ToPagedList(callPageNumber, sizeOfPage);
            var onePageOfTexts = TextsLst.ToPagedList(textPageNumber, sizeOfPage);
            ViewBag.OnePageOfCalls = onePageOfCalls;
            ViewBag.OnePageOftexts = onePageOfTexts;
            #endregion

            return View();
        }
	}
}