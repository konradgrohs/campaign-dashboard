﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlertSense.Aspen.Client;
using AlertSense.Aspen.Common.Models;
using DevOpsDashboardSample.Models;
using System.Collections.Specialized;

namespace DevOpsDashboardSample.Controllers
{
    public class HomeController : Controller
    {


        public ActionResult Index(string guidID)
        {
            var guidIDs = new List<string>();
             guidIDs.Add("1D870FD6-0D62-4FB3-88A2-15875F2DA088");
            guidIDs.Add("5D8AC4F0-0662-44B4-BCE3-930DB3A4B2D5");
            guidIDs.Add("58B19A30-0F6B-40E5-98C7-61742F951610");
            guidIDs.Add("D37360AF-EB91-4B07-9B36-12F7F8820A45");
            guidIDs.Add("EA279C38-6722-40AB-BE43-3B6F9355A6A0");
            guidIDs.Add("280D3A15-D750-4745-90B1-142D0D158AD7");
            guidIDs.Add("D366BBBF-7604-4AEA-9D79-389ED585E449");
            guidIDs.Add("D533E66D-F283-4E6E-9A4B-18CBBDAEF8D1");
            guidIDs.Add("06932FE0-0D06-4A8D-AD14-ED93A0AD7E54");
            guidIDs.Add("C8A9A8D7-E5FF-414D-852A-BCD46C53A5D6");
            guidIDs.Add("CA67B6E1-8180-4DCF-A760-F5FD76DFB198");
            guidIDs.Add("00E8AABE-AC83-4AE6-900B-5D7B11B61D23");
            guidIDs.Add("E2BF149B-DCC3-419D-94C5-BD0BD7F27373");
            guidIDs.Add("D116CF19-6325-4DA9-96C8-C48FD5C16469");
           

            var GuidLst = new List<string>();

            var GuidQry = from g in guidIDs
                          select g;
            GuidLst.AddRange(GuidQry.Distinct());
            ViewBag.guidID = new SelectList(GuidLst);
            
            

            
            var ids = from id in guidIDs
                        select id;
            var allIds = ids;
            if (!String.IsNullOrEmpty(guidID))
            {
                ids = ids.Where(s => (s.Contains(guidID)));
            }
            Guid campaignId = Guid.Parse(ids.Last());
            var request = new CampaignReportRequest { CampaignId = campaignId };
            var client = new AspenClient();
            var reportResponses = new List<CampaignReportResponse>();
            for (int i = 0; i < allIds.Count(); i++)
            {
                campaignId = Guid.Parse(allIds.ElementAt(i));
                request = new CampaignReportRequest { CampaignId = campaignId };
                reportResponses.Add(client.Get(request));
            }
            campaignId = Guid.Parse(ids.ElementAt(0));
            request = new CampaignReportRequest { CampaignId = campaignId };
                
            
            var response = client.Get(request);
            if (response.ResponseStatus != null && response.ResponseStatus.Errors.Any())
                ViewBag.Message = "Failed to retrieve the campaign report.";
            else
                ViewBag.Message = "Campaign report retrieved successfully";

            //return View(new IndexModel { ReportResponse = response });
            return View(new IndexModel { ReportResponse = response, ReportResponses = reportResponses, guidIDs = guidIDs });
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}