﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlertSense.Aspen.Common.Models;
using System.Collections.Specialized;

namespace DevOpsDashboardSample.Models
{
    public class IndexModel
    {
        public CampaignReportResponse ReportResponse { get; set; }

        public List<CampaignReportResponse> ReportResponses { get; set; }
        
        public List<string> guidIDs { get; set; }
    }
}